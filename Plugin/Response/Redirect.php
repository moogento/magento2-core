<?php

namespace Moogento\Core\Plugin\Response;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Redirect
 * @package Moogento\Core\Plugin\Response
 */
class Redirect
{

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var State
     */
    private $state;

    /**
     * Redirect constructor.
     * @param StoreManagerInterface $storeManager
     * @param State $state
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        State $state
    ) {
        $this->storeManager = $storeManager;
        $this->state = $state;
    }

    /**
     * @param \Magento\Store\App\Response\Redirect $subject
     */
    public function beforeGetRefererUrl(\Magento\Store\App\Response\Redirect $subject)
    {
        try {
            if ($this->state->getAreaCode() == Area::AREA_ADMINHTML) {
                $this->storeManager->setCurrentStore(0);
            }
        } catch (LocalizedException $e) {}
    }

}