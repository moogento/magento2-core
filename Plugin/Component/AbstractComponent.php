<?php
namespace Moogento\Core\Plugin\Component;

use Magento\Backend\App\Action;
use Magento\Framework\App\RequestInterface;
use Moogento\Core\Helper\Data;

class AbstractComponent
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Data
     */
    private $helper;

    public function __construct(
        Action\Context $context,
        Data $helper
    ){
        $this->request  = $context->getRequest();
        $this->helper   = $helper;
    }

    public function beforePrepare(\Magento\Framework\DataObject $subject)
    {
        if($this->helper->isInstalled(Data::MAGENTO_NAME_SHIPEASY)
            && $this->helper->isProPlan(\Moogento\License\Helper\Data::NAME_SHIPEASY))
        {
            $config = $subject->getData('config');

            $listDefaultOrderGridColumns = \Moogento\Core\Helper\Ordergrid::DEFAULT_ORDER_GRID_COLUMNS;

            if (in_array($subject->getName(), $listDefaultOrderGridColumns) === false) {
                $config['resizeDefaultWidth'] = 120;
            }

            $subject->setData('config', (array)$config);
        }
    }
}
