function showSpinner(el) {
    jQuery(el).addClass('with_spinner').append('<div class="moogento_spinner"></div>');
}

function hideSpinner(el) {
    jQuery(el).removeClass('with_spinner').find('.moogento_spinner').remove();
}
