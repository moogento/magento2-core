<?php

namespace Moogento\Core\Helper;

use \Magento\Framework\App\Helper\Context;
use \Moogento\Core\Renderer\Renderer;
use \Moogento\Core\ObjectManager\CreateEntity;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    CONST MAGENTO_NAME_EXPORTEASY = 'Moogento_Exporteasy';
    CONST MAGENTO_NAME_PICKNSCAN  = 'Moogento_Picknscan';
    CONST MAGENTO_NAME_PICKPACK   = 'Moogento_Pickpack';
    CONST MAGENTO_NAME_PROFITEASY = 'Moogento_Profiteasy';
    CONST MAGENTO_NAME_SHIPEASY   = 'Moogento_Shipeasy';
    CONST MAGENTO_NAME_STOCKEASY  = 'Moogento_Stockeasy';

    /**
     * @var Renderer
     */
    protected $renderer;

    /**
     * @var CreateEntity
     */
    protected $build;

    /**
     * @var \Moogento\License\Helper\Data
     */
    private $helperLicense;

    public function __construct(
        Context $context,
        Renderer $renderer,
        CreateEntity $build,
        \Moogento\License\Helper\Data $helperLicense
    ){
        parent::__construct($context);
        $this->renderer = $renderer;
        $this->build = $build;
        $this->helperLicense    = $helperLicense;
    }


    public function buildEntity($filename, $class, array $arguments = [])
    {
        $this->renderEntity($class, $filename);
        return $this->createEntity($class, $arguments);
    }

    public function renderEntity($class, $filename)
    {
        $this->renderer->renderEntity($class, $filename);
    }

    public function createEntity($class, array $arguments = [])
    {
        return $this->build->createEntity($class, $arguments);
    }

    /**
     * @param string $moduleName
     * @return bool
     */
    public function isInstalled($moduleName)
    {
        return $this->_moduleManager->isEnabled($moduleName);
    }

    public function isPlanValid($moduleName)
    {
        return $this->helperLicense->isPlanValid($moduleName);
    }

    /**
     * @param string $moduleName
     * @return bool
     */
    public function isProPlan($moduleName){
        return $this->helperLicense->isProPlan($moduleName);
    }

    public function isStartupPlan($moduleName){
        return $this->helperLicense->isStartupPlan($moduleName);
    }

    public function isBootstrapPlan($moduleName){
        return $this->helperLicense->isBootstrapPlan($moduleName);
    }
}
