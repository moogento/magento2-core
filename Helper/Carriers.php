<?php

namespace Moogento\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\ShipmentInterface;
use Magento\Sales\Api\Data\ShipmentTrackInterface;
use Magento\Sales\Api\Data\ShipmentTrackInterfaceFactory;
use Magento\Shipping\Model\Config as ShippingConfig;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;

class Carriers extends AbstractHelper
{
    /** @var null|array */
    protected $_carriersConfig = null;

    /** @var ShipmentTrackInterfaceFactory  */
    protected $trackFactory;

    /** @var ShippingConfig */
    protected $shippingConfig;

    /** @var OrderRepositoryInterface */
    protected $orderRepository;

    /** @var DirectoryList */
    protected $directoryList;

    public function __construct(
        Context $context,
        ShipmentTrackInterfaceFactory $trackInterfaceFactory,
        ShippingConfig $shippingConfig,
        OrderRepositoryInterface $orderRepository,
        DirectoryList $directoryList
    ) {
        $this->trackFactory     = $trackInterfaceFactory;
        $this->shippingConfig   = $shippingConfig;
        $this->orderRepository  = $orderRepository;
        $this->directoryList    = $directoryList;

        parent::__construct($context);
    }

    /**
     * @param ShipmentInterface $shipment
     * @param string            $trackingNumber
     * @param bool              $carrier
     * @param bool              $onlyCreateModel
     *
     * @return ShipmentTrackInterface
     */
    public function addTrackingToShipment(
        ShipmentInterface $shipment,
        $trackingNumber,
        $carrier = false,
        $onlyCreateModel = false
    ) {
        $order = $this->orderRepository->get($shipment->getOrderId());

        /** @var ShipmentTrackInterface $track */
        $track = $this->trackFactory
                      ->create()
                      ->setData($this->getTrackingData($order, $trackingNumber, $carrier))
                      ->setNumber($trackingNumber);

        if (!$onlyCreateModel) {
            $shipment->setTracks($track);
        }

        return $track;
    }

    /**
     * @param OrderInterface $order
     * @param $trackingNumber
     * @param bool $carrier
     *
     * @return string[]
     */
    public function getTrackingData(
        OrderInterface $order,
        $trackingNumber,
        $carrier = false
    ) {
        $carrierInstances = $this->shippingConfig->getAllCarriers(
            $order->getStoreId()
        );

        $carrierConfig = $this->getCarriersConfig();

        $carrierCode = 'custom';
        $title = $order->getCourierrulesDescription() ?
            $order->getCourierrulesDescription() : $order->getShippingDescription();

        if ($carrier) {
            if ($carrierConfig && isset($carrierConfig[$carrier])) {
                $title = $carrierConfig[$carrier]['title'];
                if (isset($carrierInstances[strtolower($carrier)])) {
                    $carrierCode = strtolower($carrier);
                }
            }
        } else if (!empty($carrierConfig)) {
            $carrierInfo = $this->getCarrierForTrackingNumber($trackingNumber);
            if ($carrierInfo) {
                $title = $carrierInfo['title'];
                if (isset($carrierInstances[strtolower($carrierInfo['carrier'])])) {
                    $carrierCode = strtolower($carrierInfo['carrier']);
                }
            }
        } else {
            $method = explode('_', $order->getShippingMethod());
            $method = $method[0];
            foreach ($carrierInstances as $code => $carrier) {
                if ($carrier->isTrackingAvailable() && $code == $method) {
                    $carrierCode = $code;
                    $title       = $carrier->getConfigData('title');
                }
            }
        }

        return [
            'title' => $title,
            'carrier_code' => $carrierCode,
        ];
    }

    /**
     * @param $track
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function getTrackLinkData($track)
    {
        $carrierInfo = $this->getCarrierForTrackingNumber($track->getNumber());
        $data = [
            'id' => $track->getId(),
            'title' => $this->getDefaultTitle(),
            'code'   => '',
            'number' => $track->getNumber(),
            'url' => $this->getDefaultLink(),
        ];

        if ($carrierInfo) {
            $data['code'] = $carrierInfo['code'];
            $data['url'] = (!empty($carrierInfo['link'])) ? $carrierInfo['link'] : $this->getDefaultLink();
            $data['title'] = (!empty($carrierInfo['title'])) ? $carrierInfo['title'] : $this->getDefaultTitle();
            if (!empty($carrierInfo['file'])) {
                $data['image'] = '<img src="' . $this->directoryList->getPath('media') . 'moogento/core/carriers/'
                    . $carrierInfo['file'] . '" class="szy_grid_image" alt="' . $data['title'] . '" />';
            }
        }

        $data['url'] = str_replace('#tracking#', $data['number'], $data['url']);
        $postcode = $track->getShipment()->getOrder()->getShippingAddress()->getPostcode();
        $data['url'] = str_replace('#zipcode#', $postcode, $data['url']);
        $data['url'] = str_replace('#postcode#', $postcode, $data['url']);

        return $data;
    }

    /**
     * @param string $trackingNumber
     * @return bool|
     */
    public function getCarrierForTrackingNumber($trackingNumber)
    {
        foreach ($this->getCarriersConfig() as $carrierCode => $carrierInfo) {
            if ($carrierCode) {
                $lowerCode = strtolower($carrierCode);
                $result = $carrierInfo['length'] == strlen($trackingNumber);

                if ((isset($carrierInfo['length']) && $carrierInfo['length'] ? $result : true)) {
                    if (strpos(strtolower($trackingNumber), $lowerCode) === 0
                        || strpos($lowerCode, 'length_') === 0) {
                        return $carrierInfo;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return array|null
     */
    public function getCarriersConfig()
    {
        if (is_null($this->_carriersConfig)) {
            $this->_carriersConfig = [];
            $textConfig = $this->scopeConfig->getValue('moogento_carriers/formats/list');
            if (trim($textConfig)) {
                try {
                    $textConfig = unserialize(trim($textConfig));
                    if (!is_array($textConfig)) {
                        $textConfig = [];
                    }
                    $this->fillCarrierConfig($textConfig);
                } catch (\Exception $e) {
                    $this->_logger->error($e->getMessage());
                }
            }
        }

        return $this->_carriersConfig;
    }

    /**
     * @param array $arg1
     * @param array $arg2
     *
     * @return int
     */
    protected function _sortMethod($arg1, $arg2)
    {
        if (isset($arg1['sort_order']) && $arg2['sort_order']) {
            return $arg1['sort_order'] <= $arg2['sort_order'] ? 1 : -1;
        }
        return 0;
    }

    /**
     * @return bool|string
     */
    public function getDefaultLink()
    {
        $carriersConfig = $this->getCarriersConfig();

        if (is_array($carriersConfig)) {
            foreach($carriersConfig as $carrierCode => $carrierConfigData) {
                $carrierCode = strtolower($carrierCode);
                if($carrierCode == 'default' || $carrierCode == 'custom')
                {
                    return $carrierConfigData['link'];
                }
            }
        }

        if($this->scopeConfig->getValue('moogento_carriers/general/default_link')) {
            return $this->scopeConfig->getValue('moogento_carriers/general/default_link');
        }

        return false;
    }

    /**
     * @return string
     */
    public function getDefaultTitle()
    {
        $carriersConfig = $this->getCarriersConfig();
        if (is_array($carriersConfig)) {
            foreach($carriersConfig as $carrierCode => $carrierConfigData) {
                $carrierCode = strtolower($carrierCode);
                if($carrierCode == 'default' || $carrierCode == 'custom')
                {
                    return $carrierConfigData['title'];
                }
            }
        }
        if($this->scopeConfig->getValue('moogento_carriers/general/default_label')) {
            return $this->scopeConfig->getValue('moogento_carriers/general/default_label');
        }

        return 'Custom';
    }

    public function getTrackCode($track)
    {
        $carriersConfig = $this->getCarriersConfig();

        if(is_array($carriersConfig))
        {
            foreach($carriersConfig as $internalCode => $carrierInfo) {
                if ($internalCode && strpos(strtolower($track->getNumber()), strtolower($internalCode)) !== false) {
                    return $internalCode;
                }
            }
            return strtoupper($track->getCarrierCode());
        }

        return '';
    }

    public function getTrackUrl($track)
    {
        $carrierCode = $track->getCarrierCode();
        $carriersConfig = $this->getCarriersConfig();

        $url = '';

        if ($carrierCode == 'custom') {
            $found = false;

            foreach($carriersConfig as $prefix => $carrierData) {
                if ($carrierData['title'] == $track->getTitle()) {
                    $url = str_replace(
                        '#tracking#',
                        $track->getNumber(),
                        $carrierData['link']
                    );
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $url = str_replace(
                    '#tracking#',
                    $track->getNumber(),
                    $this->getDefaultLink()
                );
            }
        }
        else {
            $url = str_replace(
                '#tracking#',
                $track->getNumber(),
                $this->getDefaultLink()
            );
            foreach($carriersConfig as $prefix => $carrierData) {
                if (strtolower($carrierData['title']) == $carrierCode) {
                    $url = str_replace(
                        '#tracking#',
                        $track->getNumber(),
                        $carrierData['link']
                    );
                    break;
                }
            }
        }
        return $url;
    }

    /**
     * @param array $textConfig
     */
    private function fillCarrierConfig(array $textConfig)
    {
        foreach($textConfig as $carrierConfig) {
            if (isset($carrierConfig['enable']) && $carrierConfig['enable']) {
                if (trim($carrierConfig['code'])) {
                    $this->_carriersConfig[ trim($carrierConfig['code']) ] = $carrierConfig;
                } elseif ($carrierConfig['length']) {
                    $this->_carriersConfig[ 'length_' . $carrierConfig['length'] ] = $carrierConfig;
                }
            }
        }
    }
}
