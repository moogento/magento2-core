<?php
namespace Moogento\Core\Helper;

class Ordergrid
{

    CONST DEFAULT_ORDER_GRID_COLUMNS = [
        'ids',
        'store_id',
        'created_at',
        'billing_name',
        'shipping_name',
        'base_grand_total',
        'grand_total',
        'status',
        'billing_address',
        'shipping_address',
        'shipping_information',
        'customer_email',
        'customer_group',
        'subtotal',
        'shipping_and_handling',
        'customer_name',
        'payment_method',
        'total_refunded'
    ];

}
