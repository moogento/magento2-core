<?php

namespace Moogento\Core\Renderer;

class Renderer extends \Magento\Framework\View\TemplateEngine\Php
{
    
    protected $objectManager;
    protected $block;
    
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Moogento\Core\Block\Block $block
    ) {
        $this->objectManager = $objectManager;
        $this->block = $block;
        parent::__construct($objectManager);
    }

    public function renderEntity($class, $fileName)
    {
        if (!class_exists($class)) {
            return $this->render(
                $this->block,
                $fileName
            );
        }
    }
    
    public function __call($name, $args)
    {
    }
    
    public function __isset($name)
    {
    }
    
    public function __get($name)
    {
    }
}
