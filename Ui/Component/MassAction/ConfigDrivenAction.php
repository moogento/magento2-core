<?php

namespace Moogento\Core\Ui\Component\MassAction;

class ConfigDrivenAction extends \Magento\Ui\Component\Action
{
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $components = [],
        array $data = [],
        $actions = null
    ) {
        parent::__construct($context, $components, $data, $actions);
        $this->scopeConfig = $scopeConfig;
    }

    public function prepare()
    {
        parent::prepare();

        $configXmlPath = $this->getData('config_xml_path');
        if ($configXmlPath) {
            $isShown = $this->scopeConfig->isSetFlag($configXmlPath);

            if (!$isShown) {
                $config = $this->getData('config');
                $config['actionDisable'] = true;
                $this->setData('config', $config);
            }
        }

    }
}
