<?php

declare(strict_types=1);

namespace Moogento\Core\Service\ColumnSetup;

use Magento\Framework\ObjectManagerInterface;

class ColumnDataProviderFactory
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectMananger = null;

    /**
     * @var string
     */
    protected $_instanceName = null;

    /**
     * CollectionFactoryFactory constructor.
     * @param ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = '\\Moogento\\Shipeasy\\Service\\ColumnSetup\\ColumnDataProvider'
    ) {
        $this->_objectMananger = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * @param array $data
     * @return \Moogento\Shipeasy\Service\ColumnSetup\ColumnDataProvider
     */
    public function create(array $data = [])
    {
        return $this->_objectMananger->create($this->_instanceName, $data);
    }
}
