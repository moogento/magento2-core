<?php

namespace Moogento\Core\Service;

use Magento\Framework\Module\ModuleList;

class MoogentoModuleList
{
    protected $moduleList;
    protected $names;

    public function __construct(ModuleList $moduleList)
    {
        $this->moduleList = $moduleList;
    }

    public function getNames()
    {
        if (null === $this->names) {
            $this->names = [];
            $names = $this->moduleList->getNames();

            foreach ($names as $name) {
                if (strpos($name, 'Moogento') === 0) {
                    $this->names[] = $name;
                }
            }

        }
        return $this->names;
    }

    public function __toString()
    {
        return implode(',', $this->getNames());
    }
}
