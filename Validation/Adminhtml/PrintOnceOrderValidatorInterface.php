<?php

namespace Moogento\Core\Validation\Adminhtml;

use Magento\Sales\Api\Data\OrderInterface;

interface PrintOnceOrderValidatorInterface
{
    /**
     * Check if module already printed
     *
     * @param OrderInterface    $order
     * @param string            $initiator
     * @return bool
     */
    public function validate(OrderInterface $order, $initiator);
}
