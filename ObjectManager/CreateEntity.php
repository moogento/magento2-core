<?php

namespace Moogento\Core\ObjectManager;

use Magento\Framework\ObjectManager\ObjectManager;

class CreateEntity extends ObjectManager
{
    public function createEntity($class, array $arguments = [])
    {
        return $this->create($class, $arguments);
    }
}
