<?php

namespace Moogento\Core\ObjectManager\Factory;

class Compiled extends \Magento\Framework\ObjectManager\Factory\Compiled
{
    protected $encodedArgs = [
        'Moogento\License\Helper\Data' =>
            [
                ['_i_' => 'Magento\Framework\App\Helper\Context'],
                ['_i_' => 'Magento\Framework\App\CacheInterface'],
                ['_i_' => 'Magento\Framework\HTTP\ZendClientFactory'],
                ['_i_' => 'Magento\Framework\ObjectManagerInterface'],
                ['_i_' => 'Magento\Framework\Module\Dir\Reader'],
                ['_i_' => 'Magento\Framework\Filesystem\Directory\ReadFactory'],
                ['_i_' => 'Moogento\License\Helper\Url'],
                ['_i_' => 'Magento\Framework\App\ProductMetadataInterface'],
                ['_i_' => 'Magento\Backend\Model\UrlInterface'],
                ['_i_' => 'Magento\Framework\Json\Helper\Data'],
            ],
        'Moogento\PickPack\Helper\Pdf\Placeholder' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Moogento\PickPack\Helper\Data'],
        ],
        'Moogento\PickPack\Helper\Pdf\Generator' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Moogento\PickPack\Model\Pdf\TemplateFactory'],
            ['_i_' => 'Moogento\PickPack\Model\Pdf\EmptyTemplateFactory'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Sales\Api\InvoiceRepositoryInterface'],
            ['_i_' => 'Magento\Sales\Api\ShipmentRepositoryInterface'],
            ['_i_' => 'Magento\Sales\Api\CreditmemoRepositoryInterface'],
            ['_i_' => 'Magento\Framework\View\Element\BlockFactory'],
            ['_i_' => 'Magento\Framework\ObjectManagerInterface'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Magento\Framework\Module\Dir\Reader'],
            ['_i_' => 'Moogento\PickPack\PdfTools\Processor'],
            ['_i_' => 'Moogento\PickPack\Helper\Data'],
            ['_i_' => 'Magento\Framework\Stdlib\DateTime\DateTime'],
            ['_i_' => 'Magento\Framework\Module\Dir\Reader'],
            ['_i_' => 'Moogento\Core\Helper\Data'],
            ['_i_' => 'Moogento\PickPack\Helper\Pdf\GenerateBySplitAttributes'],
            ['_i_' => 'Moogento\PickPack\Service\Sales\Order\Item\Sorter\SorterFactory']
        ],
        'Moogento\PickPack\Helper\Pdf\Attachment' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\PickPack\Helper\Pdf\Generator\Proxy'],
            ['_i_' => 'Magento\Checkout\Model\SessionFactory'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Customer\Model\SessionFactory']
        ],
        'Moogento\PickPack\Block\Adminhtml\System\Config\Hint' => [
            ['_i_' => 'Magento\Backend\Block\Template\Context'],
            ['_i_' => 'Moogento\PickPack\Helper\Moo'],
        ],
        'Moogento\PickPack\Helper\Moo' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Magento\Framework\App\ProductMetadataInterface'],
            ['_i_' => 'Magento\Framework\Module\PackageInfo'],
            ['_i_' => 'Magento\Framework\Module\ResourceInterface'],
            ['_i_' => 'Magento\Backend\Model\Auth\Session'],
            ['_i_' => 'Magento\Framework\Json\Helper\Data'],
            ['_i_' => 'Moogento\License\Helper\Data'],
        ],
        'Moogento\Shipeasy\Block\Adminhtml\System\Config\Hint' => [
            ['_i_' => 'Magento\Backend\Block\Template\Context'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Moo'],
        ],
        'Moogento\Shipeasy\Helper\Moo' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Magento\Framework\App\ProductMetadataInterface'],
            ['_i_' => 'Magento\Framework\Module\PackageInfo'],
            ['_i_' => 'Magento\Backend\Model\Auth\Session'],
            ['_i_' => 'Moogento\License\Helper\Data'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Index' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Invoice' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\CollectionFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Service\UpdateStatus\InvoiceService'],
            ['_i_' => 'Moogento\Shipeasy\Service\UpdateStatus\StatusDtoFactory'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Shipinvoice' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\CollectionFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Service\UpdateStatus\InvoiceService'],
            ['_i_' => 'Moogento\Shipeasy\Service\UpdateStatus\StatusDtoFactory'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Status' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\CollectionFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Updatecall' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Updateflag' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
        ],
        'Moogento\Shipeasy\Controller\Adminhtml\Shipemt\Timezoneupdate' => [
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\CollectionFactory'],
            ['_i_' => 'Magento\Framework\App\ResourceConnection'],
            ['_i_' => 'Moogento\Shipeasy\Model\ResourceModel\ShipeasyTimezoneTimezone'],
            ['_i_' => 'Moogento\License\Helper\Data'],
        ],
        'Moogento\Shipeasy\Ui\Component\Listing\Column\Address' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Escaper'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Magento\Directory\Model\Config\Source\Country'],
            ['_i_' => 'Magento\Directory\Model\CountryFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Shipeasy\Ui\Component\Listing\Column\Billingaddress' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Escaper'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Magento\Directory\Model\Config\Source\Country'],
            ['_i_' => 'Magento\Directory\Model\CountryFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Shipeasy\Ui\Component\Listing\Column\Customeremail' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Shipeasy\Ui\Component\Listing\Column\Status' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Escaper'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Shipeasy\Ui\Component\Listing\Column\Store' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Store\Model\System\Store'],
            ['_i_' => 'Magento\Framework\Escaper'],
            ['_i_' => 'Moogento\Shipeasy\Helper\Data'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Profiteasy\Block\Adminhtml\Dashboard\Dashboard' => [
            ['_i_' => 'Magento\Backend\Block\Template\Context'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'data' => ['_v_' => []],
        ],
        'Moogento\Profiteasy\Block\Adminhtml\Order\View\Custom' => [
            ['_i_' => 'Magento\Backend\Block\Template\Context'],
            ['_i_' => 'Magento\Sales\Model\ResourceModel\Order\CollectionFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Magento\Sales\Model\OrderFactory'],
            ['_i_' => 'Magento\Sales\Api\OrderRepositoryInterface'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Moogento\Profiteasy\Helper\Data'],
            ['_i_' => 'Moogento\Profiteasy\Helper\Profit'],
            'data' => ['_v_' => []]
        ],
        'Moogento\Profiteasy\Helper\Moo' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Magento\Framework\App\ProductMetadataInterface'],
            ['_i_' => 'Magento\Framework\Module\PackageInfo'],
            ['_i_' => 'Magento\Framework\Module\ResourceInterface'],
            ['_i_' => 'Magento\Backend\Model\Auth\Session'],
            ['_i_' => 'Magento\Framework\Json\Helper\Data'],
            ['_i_' => 'Moogento\License\Helper\Data']
        ],
        'Moogento\Profiteasy\Helper\Profit' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Magento\Framework\ObjectManagerInterface'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Profiteasy\Helper\Data'],
        ],
        'Moogento\Stockeasy\Helper\Moo' => [
            ['_i_' => 'Magento\Framework\App\Helper\Context'],
            ['_i_' => 'Magento\Framework\App\ProductMetadataInterface'],
            ['_i_' => 'Magento\Framework\Module\PackageInfo'],
            ['_i_' => 'Magento\Backend\Model\Auth\Session'],
            ['_i_' => 'Moogento\License\Helper\Data'],
        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Export\MassExportCsv' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Catalog\Controller\Adminhtml\Product\Builder'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Catalog\Model\ResourceModel\Product\CollectionFactory'],
            ['_i_' => 'Magento\Framework\App\Response\Http\FileFactory'],
            ['_i_' => 'Magento\Framework\App\Config\ScopeConfigInterface'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
            ['_i_' => 'Moogento\Core\Helper\Data'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Magento\Customer\Model\Session'],
            ['_i_' => 'Magento\Catalog\Model\ResourceModel\Eav\Attribute'],
        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Export\MassExportPdf' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Catalog\Controller\Adminhtml\Product\Builder'],
            ['_i_' => 'Magento\Catalog\Model\Indexer\Product\Price\Processor'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Catalog\Model\ResourceModel\Product\CollectionFactory'],
            ['_i_' => 'Magento\Framework\App\Response\Http\FileFactory'],
            ['_i_' => 'Magento\Framework\App\Config\ScopeConfigInterface'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\CatalogInventory\Api\StockRegistryInterface'],
            ['_i_' => 'Moogento\Stockeasy\Model\StockeasyproductstockdataRepository'],
            ['_i_' => 'Magento\Framework\Pricing\Helper\Data'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Moogento\Core\Helper\Data'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Qty\Add' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Catalog\Controller\Adminhtml\Product\Builder'],
            ['_i_' => 'Magento\Catalog\Model\Indexer\Product\Price\Processor'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Catalog\Model\ResourceModel\Product\CollectionFactory'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Qty\AddQty' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\CatalogInventory\Api\StockRegistryInterface'],
            ['_i_' => 'Moogento\Stockeasy\Model\StockeasyproductstockdataRepository'],
            ['_i_' => 'Moogento\License\Helper\Data'],

        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Qty\Update' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Catalog\Controller\Adminhtml\Product\Builder'],
            ['_i_' => 'Magento\Catalog\Model\Indexer\Product\Price\Processor'],
            ['_i_' => 'Magento\Ui\Component\MassAction\Filter'],
            ['_i_' => 'Magento\Catalog\Model\ResourceModel\Product\CollectionFactory'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
        ],
        'Moogento\Stockeasy\Controller\Adminhtml\Qty\UpdateQty' => [
            ['_i_' => 'Magento\Backend\App\Action\Context'],
            ['_i_' => 'Magento\Framework\View\Result\PageFactory'],
            ['_i_' => 'Moogento\Stockeasy\Helper\Data'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\CatalogInventory\Api\StockRegistryInterface'],
            ['_i_' => 'Moogento\Stockeasy\Model\StockeasyproductstockdataRepository'],
            ['_i_' => 'Moogento\License\Helper\Data'],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Catalog\Ui\Component\ColumnFactory'],
            'attributeRepository' => ['_i_' => 'Magento\Catalog\Ui\Component\Listing\Attribute\RepositoryInterface'],
            'licenseHelper' => ['_i_' => 'Moogento\License\Helper\Data'],
            'scopeConfig' => ['_i_' => 'Magento\Framework\App\Config\ScopeConfigInterface'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Cost' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            'stockeasycolumnFactory' => ['_i_' => 'Moogento\Stockeasy\Model\StockeasycolumnFactory'],
            'currency' => ['_i_' => 'Magento\Directory\Model\Currency'],
            'productRepository' => ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            'urlBuilder' => ['_i_' => 'Magento\Backend\Model\UrlInterface'],
            'licenseHelper' => ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Price' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Moogento\Stockeasy\Model\StockeasycolumnFactory'],
            ['_i_' => 'Magento\Directory\Model\Currency'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\Backend\Model\UrlInterface'],
            'urlBuilder' => ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Qty' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Moogento\Stockeasy\Model\StockeasycolumnFactory'],
            ['_i_' => 'Magento\CatalogInventory\Api\StockRegistryInterface'],
            ['_i_' => 'Magento\Backend\Model\UrlInterface'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Status' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Catalog\Model\Product\Attribute\Source\Status'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Thumbnail' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'uiComponentFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            'imageHelper' => ['_i_' => 'Magento\Catalog\Helper\Image'],
            'urlBuilder' => ['_i_' => 'Magento\Framework\UrlInterface'],
            'scopeConfig' => ['_i_' => 'Magento\Framework\App\Config\ScopeConfigInterface'],
            'assetRepo' => ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            'productRepository' => ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            'licenseHelper' => ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Stockeasy\Ui\Component\Listing\Columns\Visibility' => [
            'context' => ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            'columnFactory' => ['_i_' => 'Magento\Framework\View\Element\UiComponentFactory'],
            ['_i_' => 'Magento\Framework\Locale\CurrencyInterface'],
            ['_i_' => 'Magento\Store\Model\StoreManagerInterface'],
            ['_i_' => 'Magento\Catalog\Model\ProductRepository'],
            ['_i_' => 'Magento\Framework\View\Asset\Repository'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ],
        'Moogento\Core\Ui\Component\Columns' => [
            ['_i_' => 'Magento\Framework\View\Element\UiComponent\ContextInterface'],
            ['_i_' => 'Magento\Framework\App\Config\ScopeConfigInterface'],
            ['_i_' => 'Moogento\Core\Service\ColumnSetup\ColumnDataProviderFactory'],
            ['_i_' => 'Moogento\License\Helper\Data'],
            'components' => ['_v_' => []],
            'data' => ['_v_' => []],
        ]
    ];

    /**
     * Create instance with call time arguments
     *
     * @param string $requestedType
     * @param array $arguments
     *
     * @return object
     * @throws \Exception
     * @SuppressWarnings(Generic.Metrics.CyclomaticComplexity.TooHigh)
     */
    // @codingStandardsIgnoreStart
    public function create($requestedType, array $arguments = [])
    {
        if (isset($this->encodedArgs[$requestedType])) {
            $args = $this->encodedArgs[$requestedType];
            foreach ($args as $key => $value) {
                if (isset($value['_i_'])) {
                    $args[$key]['_i_'] = $this->config->getPreference(
                        $value['_i_']
                    );
                }
            }

            $type = $this->config->getInstanceType($requestedType);

            if (!$args) {
                return new $type();
            }

            foreach ($args as $key => &$argument) {
                if (isset($arguments[$key])) {
                    $argument = $arguments[$key];
                } elseif (isset($argument['_i_'])) {
                    $argument = $this->get($argument['_i_']);
                } elseif (isset($argument['_ins_'])) {
                    $argument = $this->create($argument['_ins_']);
                } elseif (isset($argument['_v_'])) {
                    $argument = $argument['_v_'];
                } elseif (isset($argument['_vac_'])) {
                    $argument = $argument['_vac_'];
                    $this->parseArray($argument);
                } elseif (isset($argument['_vn_'])) {
                    $argument = null;
                } elseif (isset($argument['_a_'])) {
                    if (isset($this->globalArguments[$argument['_a_']])) {
                        $argument = $this->globalArguments[$argument['_a_']];
                    } else {
                        $argument = $argument['_d_'];
                    }
                }
            }

            $args = array_values($args);

            return $this->createObject($type, $args);
        }

        return parent::create($requestedType, $arguments);
    }

    // @codingStandardsIgnoreEnd
}
